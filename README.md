### DevOps Middle тестовое 

https://www.youtube.com/watch?v=BPfBKs8tSoQ 


выбор проекта на django 
https://gitlab.com/jsdeemon/udemy_clone_backend 

### Стек девопс мидла 
- Linux Ubuntu, KVM, LVC, Docker, Ansible, Jenkins, K8s, GitLab CI 


## Заданиа на DevOps middle 

- взять любой open source проект на php(Laravel)/python(Django), форкнуть его, поднять на вашем сервере, упаковать в докер и сделать ci gitlab (так чтоб можно было комминтуть и посмотреть как изменения приехали на сервер автоматически) 

### Декомпозируем
- взять проект на django 
- запустить проект
- упаковать в Docker
- сделать Gitlab CI:
  -- CICD пайплайн - должен обновить докер образ приложения -- запустить на сервере 



### Пример проекта
https://github.com/MicroPyramid/cicd-tutorial 

- делаем тестовую стадию в рамках самого Gitlab CI 
- стадию деплоя на боевой сервер 